package cleansched.domain;

import java.time.LocalTime;
import javax.persistence.*;

/**
 *
 * @author thomas
 */
@Embeddable
public class CleaningRule {

    private LocalTime start;
    private LocalTime end;
    @Column(name = "max_interval")
    private Integer interval;

    public CleaningRule() {
    }

    public CleaningRule(LocalTime start, LocalTime end, Integer interval) {
        this.start = start;
        this.end = end;
        this.interval = interval;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }
    
    public boolean isActive() {
        LocalTime now = LocalTime.now();
        return start.isBefore(now) && end.isAfter(now);
    }
}
