package cleansched.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

/**
 *
 * @author thomas
 */
@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class CleaningEvent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "ts")
    private LocalDateTime when = LocalDateTime.now();
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person who;
    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room where;

    public CleaningEvent() {
    }

    public CleaningEvent(Person who, Room where) {
        this.who = who;
        this.where = where;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getWhen() {
        return when;
    }

    public void setWhen(LocalDateTime when) {
        this.when = when;
    }

    public Person getWho() {
        return who;
    }

    public void setWho(Person who) {
        this.who = who;
    }

    public Room getWhere() {
        return where;
    }

    public void setWhere(Room where) {
        this.where = where;
    }
}
