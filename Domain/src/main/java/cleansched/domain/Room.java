package cleansched.domain;

import com.fasterxml.jackson.annotation.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;
import javax.persistence.*;

/**
 *
 * @author thomas
 */
@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Room implements Listable {

    public enum State {
        RED {
            @Override
            public boolean matches(Optional<CleaningEvent> lastEvent, CleaningRule rule) {
                return rule.isActive() && (lastEvent.isEmpty()
                        || lastEvent.get().getWhen().isBefore(LocalDateTime.now()
                                .minusMinutes(rule.getInterval())));
            }
        }, YELLOW {
            @Override
            public boolean matches(Optional<CleaningEvent> lastEvent, CleaningRule rule) {
                return rule.isActive() && lastEvent.get().getWhen().isBefore(LocalDateTime.now()
                        .minusMinutes(rule.getInterval() - 5));
            }
        }, GREEN {
            @Override
            public boolean matches(Optional<CleaningEvent> lastEvent, CleaningRule rule) {
                return rule == null || !rule.isActive();
            }
        };

        public abstract boolean matches(Optional<CleaningEvent> lastEvent, CleaningRule rule);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "where")
    @JsonIgnore
    private List<CleaningEvent> events;
    @Embedded
    private CleaningRule rule;

    public Room() {
    }

    public Room(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CleaningEvent> getEvents() {
        if (events == null) {
            return List.of();
        }
        return events;
    }

    public void setEvents(List<CleaningEvent> events) {
        this.events = events;
    }

    public CleaningRule getRule() {
        return rule;
    }

    public void setRule(CleaningRule rule) {
        this.rule = rule;
    }

    public State getState() {
        Optional<CleaningEvent> lastEvent = getLastEvent();
        return Stream.of(State.values())
                .filter(s -> s.matches(lastEvent, rule))
                .findFirst().orElse(State.GREEN);
    }

    public Optional<CleaningEvent> getLastEvent() {
        return getEvents().stream()
                .reduce((a, b) -> a.getWhen().isAfter(b.getWhen()) ? a : b);
    }
}
