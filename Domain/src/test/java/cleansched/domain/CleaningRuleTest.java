package cleansched.domain;

import java.time.LocalTime;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author thomas
 */
public class CleaningRuleTest {
    
    public CleaningRuleTest() {
    }

    @Test
    public void ruleShouldNotBeActiveWhenOutsideInterval() {
        var testObject = new CleaningRule(
                LocalTime.now().minusHours(2),
                LocalTime.now().minusHours(1),
                30);
        assertFalse(testObject.isActive());
    }
    
    @Test
    public void ruleShouldBeActiveWhenInsideInterval() {
        var testObject = new CleaningRule(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1),
                30);
        assertTrue(testObject.isActive());
    }
}
