package cleansched.domain;

import java.time.*;
import java.util.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author thomas
 */
public class RoomTest {

    private Room testObject;

    @BeforeEach
    public void init() {
        testObject = new Room();
    }

    @Test
    public void roomWithoutRuleShouldReturnGreen() {
        assertEquals(Room.State.GREEN, testObject.getState());
    }

    @Test
    public void roomWithInactiveRuleShouldReturnGreen() {
        testObject.setRule(new CleaningRule(
                LocalTime.now().minusHours(2),
                LocalTime.now().minusHours(1),
                10));
        assertEquals(Room.State.GREEN, testObject.getState());
    }

    @Test
    public void roomWithActiveRuleAndNoEventShouldBeRed() {
        testObject.setRule(new CleaningRule(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1),
                10));
        assertEquals(Room.State.RED, testObject.getState());
    }
    
    @Test
    public void roomWithActiveRuleAndEventWithinTimeShouldBeGreen() {
        testObject.setRule(new CleaningRule(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1),
                20));
        testObject.setEvents(List.of(createCleaningEvent(LocalDateTime.now().minusMinutes(7))));
        assertEquals(Room.State.GREEN, testObject.getState());
    }
    
    @Test
    public void roomWithActiveRuleAndEventOutofTimeShouldBeRed() {
        testObject.setRule(new CleaningRule(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1),
                20));
        testObject.setEvents(List.of(createCleaningEvent(LocalDateTime.now().minusMinutes(30))));
        assertEquals(Room.State.RED, testObject.getState());
    }
    
    @Test
    public void roomWithActiveRuleAndEventSoonOutofTimeShouldBeYellow() {
        testObject.setRule(new CleaningRule(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1),
                20));
        testObject.setEvents(List.of(createCleaningEvent(LocalDateTime.now().minusMinutes(17))));
        assertEquals(Room.State.YELLOW, testObject.getState());
    }
    
    @Test
    public void roomWithoutEventsShouldReturnEmptyOptionalAsLast() {
        assertEquals(Optional.empty(), testObject.getLastEvent());
    }

    @Test
    public void roomWithOneEventShouldReturnThatEventAsLast() {
        var event = createCleaningEvent(LocalDateTime.now());
        testObject.setEvents(List.of(event));
        assertEquals(event, testObject.getLastEvent().get());
    }

    @Test
    public void roomWithTwoEventsShouldReturnLatestAsLast() {
        var event = createCleaningEvent(LocalDateTime.now().minusMinutes(30));
        var event2 = createCleaningEvent(LocalDateTime.now());
        testObject.setEvents(List.of(event, event2));
        assertEquals(event2, testObject.getLastEvent().get());
        testObject.setEvents(List.of(event2, event));
        assertEquals(event2, testObject.getLastEvent().get());
    }
    
    private CleaningEvent createCleaningEvent(LocalDateTime when) {
        CleaningEvent event = new CleaningEvent(new Person(), testObject);
        event.setWhen(when);
        return event;
    }
}
