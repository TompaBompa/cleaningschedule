package cleansched.domain;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author thomas
 */
public class PersonTest {

    @Test
    public void firstNameBySetterShouldAlwaysBeTrimmed() {
        var testObject = new Person();
        testObject.setFirstName(" Konrad\n");
        assertEquals("Konrad", testObject.getFirstName());
    }

    @Test
    public void lastNameBySetterShouldAlwaysBeTrimmed() {
        var testObject = new Person();
        testObject.setLastName(" \nSvensson \n");
        assertEquals("Svensson", testObject.getLastName());
    }

    @Test
    public void firstNameByConstructorShouldAlwaysBeTrimmed() {
        var testObject = new Person(0L, " Konrad\n", "");
        assertEquals("Konrad", testObject.getFirstName());
    }    

    @Test
    public void lastNameByConstructorShouldAlwaysBeTrimmed() {
        var testObject = new Person(0L, " Konrad\n", "\n Svensson\n\n");
        assertEquals("Svensson", testObject.getLastName());
    }   
    
    @Test
    public void namePropertyShouldListCompleteName() {
        var testObject = new Person(0L, " Konrad\n", "\n Svensson\n\n");
        assertEquals("Konrad Svensson", testObject.getName());
    }
}
