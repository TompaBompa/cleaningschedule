module cleaning.service {
    exports cleansched.service;
    
    requires spring.context;
    requires cleaning.repository;
    requires transitive cleaning.domain;
}
