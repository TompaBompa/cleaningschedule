package cleansched.repository;

import cleansched.domain.SystemMessage;
import java.util.*;
import org.springframework.stereotype.*;

/**
 *
 * @author thomas
 */
public enum SystemMessageRepository {

    DUMMY("This is a test message"),
    IS_ALIVE("Yes!");
    
    private final String message;

    private SystemMessageRepository(String message) {
        this.message = message;
    }
    
    public SystemMessage get() {
        return new SystemMessage(message, ordinal());
    }
}
