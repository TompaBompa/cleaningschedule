package cleansched.repository;

import cleansched.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}
