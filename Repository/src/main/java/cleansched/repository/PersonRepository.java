package cleansched.repository;

import cleansched.domain.Person;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author thomas
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
}
