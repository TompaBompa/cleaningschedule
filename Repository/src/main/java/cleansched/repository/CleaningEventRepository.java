package cleansched.repository;

import cleansched.domain.*;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface CleaningEventRepository extends JpaRepository<CleaningEvent, Long> {

    @Query("FROM CleaningEvent event WHERE event.where.id = ?1")
    public List<CleaningEvent> listByRoom(Long roomId);

    @Query("FROM CleaningEvent event WHERE event.who.id = ?1")
    public List<CleaningEvent> listByPerson(Long personId);
}
