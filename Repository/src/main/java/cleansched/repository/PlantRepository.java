package cleansched.repository;

import cleansched.domain.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author thomas
 */
@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {
}
