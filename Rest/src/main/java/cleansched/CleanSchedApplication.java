package cleansched;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanSchedApplication {

    public static void main(String[] args) {
        SpringApplication.run(CleanSchedApplication.class, args);
    }
}
