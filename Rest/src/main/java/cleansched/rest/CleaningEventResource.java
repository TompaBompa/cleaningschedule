package cleansched.rest;

import cleansched.domain.*;
import cleansched.rest.log.*;
import cleansched.service.CleaningEventService;
import cleansched.service.util.*;
import java.util.List;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("event")
public class CleaningEventResource {

    private final CleaningEventService service;
    private final ResourceLogger logger;

    @Autowired
    public CleaningEventResource(CleaningEventService service) {
        this.service = service;
        this.logger = ResourceLogger.of(ResourceLogger.Type.STANDARD);
    }

    @PostMapping("{roomId}/{personId}")
    public CleaningEvent saveEvent(@PathVariable Long roomId, @PathVariable Long personId) {
        return logAndRun(() -> service.saveEvent(roomId, personId), roomId, personId);
    }
    
    @GetMapping("room/{roomId}")
    public List<EventByRoom> listByRoom(@PathVariable Long roomId) {
        return logAndRun(() -> service.listByRoom(roomId), roomId);
    }
    
    @GetMapping("person/{personId}")
    public List<EventByPerson> listByPerson(@PathVariable Long personId) {
        return logAndRun(() -> service.listByPerson(personId), personId);
    }
    
    private <T> T logAndRun(Supplier<T> serviceMethod, Object... params) {
        logger.defaultLog(params);
        return serviceMethod.get();
    }
}
