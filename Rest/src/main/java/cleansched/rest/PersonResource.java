package cleansched.rest;

import cleansched.domain.Person;
import cleansched.service.PersonService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("person")
public class PersonResource {

    private final PersonService service;

    @Autowired
    public PersonResource(PersonService service) {
        this.service = service;
    }

    @PostMapping
    public Person savePerson(@RequestBody Person p) {
        return service.savePerson(p);
    }
    
    @GetMapping("{id}")
    public Person getPersonById(@PathVariable("id") Long id) {
        return service.getPerson(id);
    }
    
    @GetMapping
    public List<Person> getAllPersons() {
        return service.getAll();
    }
}
