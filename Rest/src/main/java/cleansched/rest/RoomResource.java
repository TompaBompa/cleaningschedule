package cleansched.rest;

import cleansched.domain.*;
import cleansched.service.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("room")
public class RoomResource {

    private final RoomService service;

    @Autowired
    public RoomResource(RoomService service) {
        this.service = service;
    }

    @PostMapping("{plantId}")
    public Room saveRoom(@PathVariable Long plantId, @RequestBody Room room) {
        return service.saveRoom(plantId, room);
    }
    
    @GetMapping("{id}")
    public Room getRoomById(@PathVariable("id") Long id) {
        return service.getRoom(id);
    }
    
    @GetMapping("plant/{plantId}")
    public List<Room> getRoomByPlantId(@PathVariable Long plantId) {
        return service.listByPlantId(plantId);
    }
    
    @GetMapping
    public List<Room> getAllRooms() {
        return service.getAll();
    }
}
