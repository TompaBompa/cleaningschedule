package cleansched.rest;

import cleansched.domain.*;
import cleansched.service.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("plant")
public class PlantResource {

    private final PlantService service;

    @Autowired
    public PlantResource(PlantService service) {
        this.service = service;
    }

    @PostMapping
    public Plant savePlant(@RequestBody Plant plant) {
        return service.savePlant(plant);
    }
    
    @GetMapping("{id}")
    public Plant getPlantById(@PathVariable("id") Long id) {
        return service.getPlant(id);
    }
    
    @GetMapping
    public List<? extends Listable> getAllPlants() {
        return service.getAll();
    }
}
