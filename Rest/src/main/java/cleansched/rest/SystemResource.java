package cleansched.rest;

import cleansched.domain.SystemMessage;
import cleansched.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("system")
public class SystemResource {

    private final SystemService service;

    @Autowired
    public SystemResource(SystemService service) {
        this.service = service;
    }

    @GetMapping("isalive")
    public SystemMessage isAlive() {
        return service.isAlive();
    }

    @GetMapping("test")
    public SystemMessage getTestMessage() {
        return service.getTestMessage();
    }
}
