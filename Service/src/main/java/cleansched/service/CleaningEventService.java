package cleansched.service;

import cleansched.domain.*;
import cleansched.repository.*;
import cleansched.service.util.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class CleaningEventService {

    private final CleaningEventRepository repository;
    private final RoomRepository roomRepository;
    private final PersonRepository personRepository;

    @Autowired
    public CleaningEventService(
            CleaningEventRepository repository,
            RoomRepository roomRepository,
            PersonRepository personRepository) {
        this.repository = repository;
        this.roomRepository = roomRepository;
        this.personRepository = personRepository;
    }

    public CleaningEvent saveEvent(Long roomId, Long personId) {
        Room room = roomRepository.getById(roomId);
        Person person = personRepository.getById(personId);
        CleaningEvent event = new CleaningEvent(person, room);
        return repository.save(event);
    }

    public List<EventByRoom> listByRoom(Long roomId) {
        return repository.listByRoom(roomId).stream()
                .map(EventByRoom::new)
                .toList();
    }

    public List<EventByPerson> listByPerson(Long personId) {
        return repository.listByPerson(personId).stream()
                .map(EventByPerson::new)
                .toList();
    }
}
