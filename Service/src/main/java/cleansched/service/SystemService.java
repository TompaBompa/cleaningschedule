package cleansched.service;

import cleansched.domain.SystemMessage;
import cleansched.repository.SystemMessageRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class SystemService {

    public SystemMessage isAlive() {
        return SystemMessageRepository.IS_ALIVE.get();
    }

    public SystemMessage getTestMessage() {
        return SystemMessageRepository.DUMMY.get();
    }
}
