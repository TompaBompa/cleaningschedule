package cleansched.service.util;

import cleansched.domain.CleaningEvent;
import java.time.*;

/**
 *
 * @author thomas
 */
public class EventByRoom {

    private final CleaningEvent delegate;

    public EventByRoom(CleaningEvent delegate) {
        this.delegate = delegate;
    }
    
    public Long getId() {
        return delegate.getId();
    }
    
    public String getWho() {
        return delegate.getWho().getName();
    }
    
    public LocalDateTime getWhen() {
        return delegate.getWhen();
    }
}
