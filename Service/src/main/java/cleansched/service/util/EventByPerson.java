package cleansched.service.util;

import cleansched.domain.CleaningEvent;
import java.time.LocalDateTime;

/**
 *
 * @author thomas
 */
public class EventByPerson {

    private final CleaningEvent delegate;

    public EventByPerson(CleaningEvent delegate) {
        this.delegate = delegate;
    }

    public Long getId() {
        return delegate.getId();
    }

    public String getWhere() {
        return delegate.getWhere().getName();
    }

    public LocalDateTime getWhen() {
        return delegate.getWhen();
    }
}
