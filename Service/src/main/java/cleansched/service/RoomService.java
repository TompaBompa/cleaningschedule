package cleansched.service;

import cleansched.domain.*;
import cleansched.repository.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class RoomService {

    private final RoomRepository repository;
    private final PlantRepository plantRepository;

    @Autowired
    public RoomService(RoomRepository repository, PlantRepository plantRepository) {
        this.repository = repository;
        this.plantRepository = plantRepository;
    }

    public Room saveRoom(Long plantId, Room room) {
        Room result = repository.save(room);
        Plant plant = plantRepository.findById(plantId).orElseThrow();
        plant.getRooms().add(room);
        plantRepository.save(plant);
        return result;
    }
    
    public Room getRoom(Long id) {
        return repository.getById(id);
    }
    
    public List<Room> getAll() {
        return repository.findAll();
    }

    public List<Room> listByPlantId(Long plantId) {
        return plantRepository.getById(plantId).getRooms();
    }
}
