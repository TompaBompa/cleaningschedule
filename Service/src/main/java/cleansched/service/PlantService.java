package cleansched.service;

import cleansched.domain.*;
import cleansched.repository.*;
import cleansched.service.util.local.ListableProxy;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class PlantService {

    private final PlantRepository repository;

    @Autowired
    public PlantService(PlantRepository repository) {
        this.repository = repository;
    }

    public Plant savePlant(Plant plant) {
        return repository.save(plant);
    }

    public Plant getPlant(Long id) {
        return repository.getById(id);
    }

    public List<? extends Listable> getAll() {
        return ListableProxy.listOf(repository.findAll());
    }
}
