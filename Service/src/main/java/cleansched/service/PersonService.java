package cleansched.service;

import cleansched.domain.Person;
import cleansched.repository.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class PersonService {

    private final PersonRepository repository;

    @Autowired
    public PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public Person savePerson(Person p) {
        if (p.getId() != null) {
            throw new ServiceException("New person object can not have ID prior to persistence");
        }
        return repository.save(p);
    }
    
    public Person getPerson(Long id) {
        return repository.getById(id);
    }
    
    public List<Person> getAll() {
        return repository.findAll();
    }
}
