module cleaning.service {
    exports cleansched.service;
    exports cleansched.service.util;
    
    requires spring.context;
    requires cleaning.repository;
    requires transitive cleaning.domain;
}
